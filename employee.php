<?php include_once "header.php"; ?>
<?php
	include_once "model/employee_model.php";
	$result = $obj->getAllEmployees();
?>
<div class="col-md-12 col-sm-12 page-header-div">
	<div class="col-md-10 col-sm-10 no-padding">
		<h3 >Employee Details</h3>
	</div>
	<div class="col-md-2 col-sm-2 no-padding">
		<h3><a href="add_employee.php" class="btn btn-primary btn-sm page-header-btn pull-right">+ Add</a></h3>
	</div>
</div>
<div class="col-md-12 col-sm-12 page-body">
<?php
	if($result == "0"){
		?>
			<div class="alert alert-warning">
			  <strong>No Data Available!</strong>
			</div>
		<?php
	}else{
		?>
			<table id="myTable" class="table table-striped table-bordered" style="width:100%">
				 <thead>
		            <tr>
		                <th>Name</th>
		                <th>Age</th>
		                <th>Mobile No</th>
		                <th>E-mail</th>
		            </tr>
		        </thead>
		        <tbody>
		        	<?php
		        		foreach($result as $row){
		        			?>
		        				<tr>
		        					<td><a href="employee_details.php?id=<?php echo $row['id']?>"><strong><?php echo $row['name']?></strong></a></td>
		        					<td><?php echo $row['age']?></td>
		        					<td><?php echo $row['contactno']?></td>
		        					<td><?php echo $row['email']?></td>
		        				</tr>

		        			<?php
		        		}
		        	?>
		        </tbody>
			</table>
		<?php
	}
?>

</div>
<?php include_once "footer.php";?>