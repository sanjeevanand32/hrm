<?php
  if(isset($_COOKIE['hrm_userid'])){
    echo "<script>window.location.href='home.php';</script>";
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Login</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Lato|Montserrat|Raleway|Vollkorn&display=swap" rel="stylesheet">
  <style>
*{
    font-family: 'Vollkorn', serif;
    font-family: 'Lato', sans-serif;
    font-family: 'Raleway', sans-serif;
    font-family: 'Montserrat', sans-serif;
  } 
  body{background: #f4f5f7;} 
  .login-form-body{
      position:fixed;
      background:#fff;
      padding:30px 40px;
      margin-top: 10vh;
      box-shadow: 0 3px 16px 0 rgba(132,132,132,0.35);
  }
  .btn-span{background: #5d2bff;padding: 8px 20px;border-radius: 20px; color: #fff; font-weight: bold;}
  .no-padding{padding: 0;}
  .form-err{list-style-type: none; display: none;}
  .form-err li{color: red;}
  </style>
</head>
<body>
<div class="col-md-12 col-sm-12">
    <div class="col-md-3 col-sm-3 col-md-offset-4 col-sm-offset-4 login-form-body">
    <h3><strong>login Here</strong></h3>
    <div class="col-md-12 col-sm-12 no-padding">
      <ul class="form-err">
        <li>Invalid Login/Password!</li>
      </ul>
    </div>
    <div class="form-group">
      <label for="uname">Username:</label>
      <input type="text" class="form-control no-curve" id="uname" placeholder="Enter Username">
    </div>
    <div class="form-group">
      <label for="pwd">Password:</label>
      <input type="password" class="form-control no-curve" id="pwd" placeholder="Enter password" name="pwd">
    </div>
    <button type="button" class="btn btn-primary" onclick=login()>Login</button>
    <h5><a href="forgot_password.php">Forgot Password?</a></h5>
    </div>
    
</div>

</body>
</html>

<script>
function login(){
  var uname = $('#uname').val();
  var pwd = $('#pwd').val();
  var is_err = validate(uname, pwd);
  var login_user = "";
  if(is_err != 1){
      $.ajax({
        url : "model/login_model.php",
        type : "POST", 
        data : {login_user, uname, pwd},
        success : function(result){
          
          if(result == 0){
            $('.form-err').show();
          }else{
            location.replace("home.php");
          }
        }

      });
  }
  
}

function validate(uname, pwd){
  var is_err = 0;
  if(uname.length == 0){
    is_err = 1;
  }

  if(pwd.length == 0){
    is_err = 1;
  }

  if(is_err == 1){
    $('.form-err').show();
  }
  return is_err;
}
</script>


