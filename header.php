<?php
  if(!isset($_COOKIE['hrm_userid'])){
    echo "<script>window.location.href='login.php';</script>";
  }
?>

<?php
  $title = "HRM";
  $baseURL = "hrm";
  $homeDir = "http://localhost/hrm";
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?php echo $title;?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Lato|Montserrat|Raleway|Vollkorn&display=swap" rel="stylesheet">
  <script defer="" src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-colvis-1.6.1/b-flash-1.6.1/b-html5-1.6.1/b-print-1.6.1/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-colvis-1.6.1/b-flash-1.6.1/b-html5-1.6.1/b-print-1.6.1/datatables.min.js"></script>
</head>


  <style>
  *{
    font-family: 'Vollkorn', serif;
    font-family: 'Lato', sans-serif;
    font-family: 'Raleway', sans-serif;
    font-family: 'Montserrat', sans-serif;
  }
  .no-curve{
      border-radius: 0;
  }
  .login-form-body{
      position:fixed;
      background:#eee;
      padding:20px;;
      margin-top: 10vh;
      box-shadow : 1px 1px 4px 4px #eee;
  }
  .no-padding{
      padding: 0;
  }
  .no-margin{margin: 0;}
 
.right-shadow
{
  box-shadow: 5px 0 5px -2px #eee;
}
.pointer:hover{cursor: pointer}



.top-nav{border-bottom: 1px solid #eee;padding: 0; padding-top: 3px; padding-bottom: 5px;}
.user-ic-div{text-align: right;padding-right: 2%;}
.user-ic{font-size: 30px;}
.left-nav{position: fixed; height: 100vh; background: #eee; z-index: 2;}
.menu-item{padding-left: 10%;}
.menu-link{padding: 3px; padding-top: 5px; padding-bottom: 5px; text-decoration : none; color: #5D2BFF; display: block; width: 100%; font-weight: bold;}
.menu-link:hover, .menu-link:focus{text-decoration : none; color: #5D2BFF;font-weight: bold;}

.modal.fade:not(.in).right .modal-dialog {
    -webkit-transform: translate3d(25%, 0, 0);
    transform: translate3d(25%, 0, 0);
}

.modal-dialog{
    position: fixed;
    height: 100vh;
    top: -5vh;
    right: 0;
    width: 25%;
}
.modal-body{
    height: 100vh;
}

.modal-backdrop {
    opacity:0 !important;
}


.page-header{margin: 0;margin-top:5px; margin-bottom: 5px; color: #5d2bff; border:0;}
//.page-header-div{background: #f4f5f7;}
.page-btn-span{background: #5d2bff;padding: 5px 20px;border-radius: 20px; color: #fff; position: absolute; top: 10%; right: 2%; font-weight: bold;}
.form-lbl{position:relative; top: 10px;}
.form-row{margin-bottom: 10px;}

.form-error {
    border: 2px solid #f28e8e;
    border-radius: 7px;
    display: none;
    color: #f28e8e;
    padding: 10px;
    font-weight: bold;

}

  </style>
</head>
<body>
<div class="col-md-12 col-sm-12 no-padding">
    <?php include_once "left_navigator.php" ?>
    <div class="col-md-2 col-sm-2"></div>
    <div class="col-md-10 col-sm-10 no-padding">
        <?php include_once "top_navigator.php"; ?>
        <div class="col-md-12 col-sm-12 no-padding main-body">