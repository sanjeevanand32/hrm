<?php
include_once "connect.php";
class EmployeesModel extends Dbconfig{
    
    public function getAllEmployees(){
    	$q = "SELECT DATE_FORMAT(profile.dob, '%d %M %Y') as dob,profile.* FROM profile";

    	$res = mysqli_query($this->conn, $q);

    	if($res){
            $result = mysqli_fetch_all($res, MYSQLI_ASSOC);
            return $result;
	    }
	}

    public function getThisEmployeeDetails($id){
        $q = "SELECT * FROM profile WHERE id=$id";
        $res = mysqli_query($this->conn, $q);

        if($res){
            $result = mysqli_fetch_array($res, MYSQLI_ASSOC);
            $cnt = mysqli_num_rows($res);
            if($cnt > 0){
                $q = "SELECT * FROM contact_persons WHERE profile_id = $id";
                $res = mysqli_query($this->conn, $q);
                $cp_result = mysqli_fetch_all($res, MYSQLI_ASSOC);
                $cnt = mysqli_num_rows($res);
                if($cnt > 0){

                    $result['c_person1'] = $cp_result[0];
                    $result['c_person2'] = $cp_result[1];
                }
                

                return $result;
            }else{
                return "0";
            }
        }
    }

    public function addNewEmployee($name,$role,$age,$gender,$dob,$father,$mother,$blood,$mobile,$email,$aadharno,$line1,$line2,$city,$state,$country,$pincode,$cp1_name,$cp1_gender,$cp1_mobile,$cp1_address,$cp2_name,$cp2_gender,$cp2_mobile,$cp2_address,$file_name1,$file_name2,$file_name3,$accno,$ifsc,$acctype,$bank,$branch){

    	$q = "INSERT INTO profile (name,role,age,father,mother,gender,dob,bloodgroup,contactno,email,aadhaar,line1,line2,city,state,country,pincode,bank_acctype,bank_accno,bank_name,bank_branch,photo_id_card,photo_aadhaar,bank_passbook,bank_ifsc) VALUES('$name','$role','$age','$father','$mother','$gender','$dob','$blood','$mobile','$email','$aadharno','$line1','$line2','$city','$state','$country','$pincode','$acctype','$accno','$bank','$branch','$file_name2','$file_name3','$file_name1','$ifsc')";

     	$res = mysqli_query($this->conn, $q);

     	if($res){
     		$employee_id = $this->conn->insert_id;
     	}

     	$q = "INSERT INTO contact_persons (profile_id,name,gender,contactno,address) VALUES ('$employee_id','$cp1_name','$cp1_gender','$cp1_mobile','$cp1_address'),('$employee_id','$cp2_name','$cp2_gender','$cp2_mobile','$cp2_address')";

     	$res = mysqli_query($this->conn, $q);

     	if($res){
     		return true;
     	}


    }

    public function updateEmployee($id,$name,$role,$age,$gender,$dob,$father,$mother,$blood,$mobile,$email,$aadharno,$line1,$line2,$city,$state,$country,$pincode,$cp1_name,$cp1_gender,$cp1_mobile,$cp1_address,$cp2_name,$cp2_gender,$cp2_mobile,$cp2_address,$file_name1,$file_name2,$file_name3,$accno,$ifsc,$acctype,$bank,$branch){

        $q = "UPDATE profile SET name='$name',role='$role',age='$age',father='$father',mother='$mother',gender='$gender',dob='$dob',bloodgroup='$blood',contactno='$mobile',email='$email',aadhaar='$aadharno',line1='$line1',line2='$line2',city='$city',state='$state',country='$country',pincode='$pincode',bank_acctype='$acctype',bank_accno='$accno',bank_name='$bank',bank_branch='$branch',photo_id_card='$file_name2',photo_aadhaar='$file_name3',bank_passbook='$file_name1',bank_ifsc='$ifsc'  WHERE id='$id'";

        $res = mysqli_query($this->conn, $q);
        if($res){
            $q = "DELETE FROM contact_persons WHERE profile_id=$id";
            $res = mysqli_query($this->conn, $q);

            $q = "INSERT INTO contact_persons (profile_id,name,gender,contactno,address) VALUES ('$id','$cp1_name','$cp1_gender','$cp1_mobile','$cp1_address'),('$id','$cp2_name','$cp2_gender','$cp2_mobile','$cp2_address')";

            $res = mysqli_query($this->conn, $q);

            if($res){
                return true;
            }

        }
    }

}

$obj = new EmployeesModel;
?>