<?php
class Dbconfig{
    protected $databaseName = "hrm_db";
    protected $hostName = "localhost";
    protected $userName = "root";
    protected $password = "";

    function __construct() {
        $this->conn = mysqli_connect($this->hostName, $this->userName, $this->password, $this->databaseName);

        if(!$this->conn) {
            die("Connection failed: " . mysqli_connect_error());
        }
    }
}
?>