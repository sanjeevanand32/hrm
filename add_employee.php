<?php  include_once "header.php";?>
<?php


include_once "model/employee_model.php";


if(isset($_POST['type']) == "add" || isset($_POST['type']) == "add"){
	
	$is_err = 0;
	$err_message = "";

	$file_name1 = "";
	if($_FILES["file_passbook"]["size"] > 0){
		$target_dir1 = "img/uploads/passbook/";
		$file_name1 = $_FILES["file_passbook"]["name"];
		$check1 = getimagesize($_FILES["file_passbook"]["tmp_name"]);
		$fileSize1 = $_FILES["file_passbook"]["size"];
		$tempName1 = $_FILES["file_passbook"]["tmp_name"];
		$target_file1 = $target_dir1 . basename($file_name1);
		$file_passbook_check = check_file($file_name1, $check1, $fileSize1, $tempName1, $target_file1);
		if($file_passbook_check != 1){
			$is_err = 1;
			$err_message .= "<li>".$file_passbook_check."</li>";
		}
	}

	$file_name2 = "";
	if($_FILES["file_idcard"]["size"] > 0){
		$target_dir2 = "img/uploads/id/";
		$file_name2 = $_FILES["file_idcard"]["name"];
		$check2 = getimagesize($_FILES["file_idcard"]["tmp_name"]);
		$fileSize2 = $_FILES["file_idcard"]["size"];
		$tempName2 = $_FILES["file_idcard"]["tmp_name"];
		$target_file2 = $target_dir2 . basename($file_name2);
		$file_idcard_check = check_file($file_name2, $check2, $fileSize2, $tempName2, $target_file2);
		if($file_idcard_check != 1){
			$is_err = 1;
			$err_message .= "<li>".$file_idcard_check."</li>";
		}
	}
	
	$file_name3 = "";
	if($_FILES["file_aadhaar"]["size"] > 0){
		$target_dir3 = "img/uploads/aadhaar/";
		$file_name3 = $_FILES["file_aadhaar"]["name"];
		$check3 = getimagesize($_FILES["file_aadhaar"]["tmp_name"]);
		$fileSize3 = $_FILES["file_aadhaar"]["size"];
		$tempName3 = $_FILES["file_aadhaar"]["tmp_name"];
		$target_file3 = $target_dir3 . basename($file_name3);
		$file_aadhaar_check = check_file($file_name3, $check3, $fileSize3, $tempName3, $target_file3);
		if($file_aadhaar_check != 1){
			$is_err = 1;
			$err_message .= "<li>".$file_aadhaar_check."</li>";
		}
	}

	
	$type = $_POST['type'];
	$name = trim($_POST['name']);
	$role = $_POST['role'];
	$age = trim($_POST['age']);
	$gender = $_POST['gender'];
	$dob = $_POST['dob'];
	$father = trim($_POST['father']);
	$mother = trim($_POST['mother']);
	$blood = $_POST['blood'];
	$mobile = trim($_POST['mobile']);
	$email = trim($_POST['email']);
	$aadharno = trim($_POST['aadharno']);

	$line1 = trim($_POST['line1']);
	$line2 = trim($_POST['line2']);
	$city = trim($_POST['city']);
	$state = trim($_POST['state']);
	$country = trim($_POST['country']);
	$pincode = trim($_POST['pincode']);

	$cp1_name = trim($_POST['cp1_name']);
	$cp1_gender = trim($_POST['cp1_gender']);
	$cp1_mobile = trim($_POST['cp1_mobile']);
	$cp1_address = trim($_POST['cp1_address']);

	$cp2_name = trim($_POST['cp2_name']);
	$cp2_gender = trim($_POST['cp2_gender']);
	$cp2_mobile = trim($_POST['cp2_mobile']);
	$cp2_address = trim($_POST['cp2_address']);

	$accno = trim($_POST['accno']);
	$ifsc = trim($_POST['ifsc']);
	$acctype = trim($_POST['acctype']);
	$bank = trim($_POST['bank']);
	$branch = trim($_POST['branch']);

	if($age == ""){
		$age = 0;
	}

	if($dob == ""){
		$dob = date("Y-m-d");
	}
	


	if($name == ""){
		$is_err = 1;
		$err_message .= "<li>Please enter employee name</li>";
	}

	if($role == "nothing"){
		$is_err = 1;
		$err_message .= "<li>Please select employee role</li>";
	}

	if($gender == "nothing"){
		$is_err = 1;
		$err_message .= "<li>Please select gender</li>";
	}

	if($cp1_name == "" && ($cp1_gender != "" || $cp1_mobile != "" || $cp1_address != "")){
		$is_err = 1;
		$err_message .= "<li>Please enter contact person name</li>";
	}

	if($cp2_name == "" && ($cp2_gender != "" || $cp2_mobile != "" || $cp2_address != "")){
		$is_err = 1;
		$err_message .= "<li>Please enter contact person name</li>";
	}



	

	if($is_err != 1){
		$err_div = "none";
		if($_FILES["file_passbook"]["size"] > 0){
			move_uploaded_file($tempName1, $target_file1);
		}

		if($_FILES["file_idcard"]["size"] > 0){
			move_uploaded_file($tempName2, $target_file2);
		}

		if($_FILES["file_aadhaar"]["size"] > 0){
			move_uploaded_file($tempName3, $target_file3);
		}

		if($type == "add"){
			$result = $obj->addNewEmployee($name,$role,$age,$gender,$dob,$father,$mother,$blood,$mobile,$email,$aadharno,$line1,$line2,$city,$state,$country,$pincode,$cp1_name,$cp1_gender,$cp1_mobile,$cp1_address,$cp2_name,$cp2_gender,$cp2_mobile,$cp2_address,$file_name1,$file_name2,$file_name3,$accno,$ifsc,$acctype,$bank,$branch);
		}else{
			$id = $_POST['employee_id'];
			$result = $obj->updateEmployee($id,$name,$role,$age,$gender,$dob,$father,$mother,$blood,$mobile,$email,$aadharno,$line1,$line2,$city,$state,$country,$pincode,$cp1_name,$cp1_gender,$cp1_mobile,$cp1_address,$cp2_name,$cp2_gender,$cp2_mobile,$cp2_address,$file_name1,$file_name2,$file_name3,$accno,$ifsc,$acctype,$bank,$branch);
		}
		if($result == 1){
			$message = "Profile Updated";
			echo "<script type='text/javascript'>alert('$message');</script>";
		}

	}else{
		$err_div = "block";
	}


}


function check_file($file_name, $check, $fileSize, $tempName, $target_file){
	

	$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
	
	// Check if image file is a actual image or fake image
	if(isset($_POST["submit"])) {
	    
	    if($check == false) {
	        return basename( $file_name)."is not image";
	    }
	}

	// Check if file already exists
	// if (file_exists($target_file)) {
	//     return basename( $file_name)." file already exists.";
	// }

	// Check file size
	if ($fileSize > 500000) {
	    return basename( $file_name)." File is too large.";
	}

	// Allow certain file formats
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
	&& $imageFileType != "gif" ) {
	    return basename( $file_name). "is invalid file format. only JPG, JPEG, PNG & GIF files are allowed.";
	}

	return 1;


}

if(isset($_GET['id'])){
	$emp_id = $_GET['id'];

	$details = $obj->getThisEmployeeDetails($emp_id);

	if($details == 0){
		?><div class="alert alert-warning" style="margin:20px;">
			<strong>No such employee found!</strong>
		</div><?php
		exit;
	}

}

?>


<div class="col-md-12 col-sm-12 page-header-div">
    <h3 class="page-header">Add Employee Here </h3>
</div>
<style>
.form-row{margin-bottom: 10px;}
</style>
<div class="col-md-12 col-sm-12 page-body">
	<form action="" method="post" enctype="multipart/form-data">
		<input type="hidden" name="employee_id" value="<?php if(isset($details)){echo $details['id'];}?>">
		<input type="hidden" name="type" value="<?php if(isset($details)){echo "edit";}else{echo "add";}?>">
		<div class="col-md-6 col-sm-6 form-error" style="display: <?php echo $err_div;?>">
			<ul class="form-err-ul">
				<?php if(isset($err_message)){echo $err_message;}; ?>
			</ul>
		</div>

		<div class="col-md-12 col-sm-12">
			 <h3>General Details</h3>
			 <div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">Employee Name *</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<input type="text" class="form-control no-curve" id="name" name="name" placeholder="Enter Full Name" value="<?php if(isset($details)){echo $details['name'];}?>">
			 		</div>
			 	</div>	
			 </div>

			 <div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">Role *</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<input type="text" class="form-control no-curve" id="role" name="role" placeholder="Enter Role" value="<?php if(isset($details)){echo $details['role'];}?>">
			 		</div>
			 	</div>	
			 </div>

			 <div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">Age</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<input type="text" class="form-control no-curve" id="age" name="age" placeholder="Enter Age" value="<?php if(isset($details)){echo $details['age'];}?>">
			 		</div>
			 	</div>	
			 </div>

			 <div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">Gender *</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<select class="form-control no-curve" id="gender" name="gender">
                            <option value="nothing">-- Select Gender --</option>
                           	<option value="male" <?php if(isset($details)){if($details['gender'] == "male"){echo "selected";}}?>>Male</option>
                           	<option value="female" <?php if(isset($details)){if($details['gender'] == "female"){echo "selected";}}?>>Female</option>
                        </select>
			 		</div>
			 	</div>	
			 </div>

			  <div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">Date Of Birth</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<input type="date" class="form-control no-curve" id="dob" name="dob" placeholder="Enter Date Of Birt" value="<?php if(isset($details)){echo $details['dob'];}?>">
			 		</div>
			 	</div>	
			 </div>

			  <div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">Father's Name</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<input type="text" class="form-control no-curve" id="father" name="father" placeholder="Enter Father's Name" value="<?php if(isset($details)){echo $details['father'];}?>">
			 		</div>
			 	</div>	
			 </div>

			  <div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">Mother's Name</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<input type="text" class="form-control no-curve" id="mother" name="mother" placeholder="Enter Mother's Name" value="<?php if(isset($details)){echo $details['mother'];}?>">
			 		</div>
			 	</div>	
			 </div>

			 <div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">Blood Group</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<select class="form-control no-curve" id="blodd" name="blood">
                                <option value="">-- Select Blood Group --</option>
                                <option value="A+" <?php if(isset($details['bloodgroup'])){if($details['bloodgroup'] == "A+"){echo "selected";}}?>>A+</option>
                                <option value="A-" <?php if(isset($details['bloodgroup'])){if($details['bloodgroup'] == "A-"){echo "selected";}}?>>A-</option>
                                <option value="B+" <?php if(isset($details['bloodgroup'])){if($details['bloodgroup'] == "B+"){echo "selected";}}?>>B+</option>
                                <option value="B-" <?php if(isset($details['bloodgroup'])){if($details['bloodgroup'] == "B-"){echo "selected";}}?>>B-</option>
                                <option value="O+" <?php if(isset($details['bloodgroup'])){if($details['bloodgroup'] == "O+"){echo "selected";}}?>>O+</option>
                                <option value="O-" <?php if(isset($details['bloodgroup'])){if($details['bloodgroup'] == "O-"){echo "selected";}}?>>O-</option>
                                <option value="AB+" <?php if(isset($details['bloodgroup'])){if($details['bloodgroup'] == "AB+"){echo "selected";}}?>>AB+</option>
                                <option value="AB-" <?php if(isset($details['bloodgroup'])){if($details['bloodgroup'] == "AB-"){echo "selected";}}?>>AB-</option>
                            </select>
			 		</div>
			 	</div>	
			 </div>

			 <div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">Mobile Number</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<input type="text" class="form-control no-curve" id="mobile" name="mobile" placeholder="Enter Mobile Number" value="<?php if(isset($details)){echo $details['contactno'];}?>">
			 		</div>
			 	</div>	
			 </div>

			 <div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">E-mail</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<input type="text" class="form-control no-curve" id="email" name="email" placeholder="Enter E-mail Address" value="<?php if(isset($details)){echo $details['email'];}?>">
			 		</div>
			 	</div>	
			 </div>

			 <div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">Aadhaar Number</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<input type="text" class="form-control no-curve" id="aadharno" name="aadharno" placeholder="Enter Aadhaar Number" value="<?php if(isset($details)){echo $details['aadhaar'];}?>">
			 		</div>
			 	</div>	
			 </div>

			<h3>Address Details</h3>

			<div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">Line 1</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<input type="text" class="form-control no-curve" id="line1" name="line1" placeholder="Address Line 1" value="<?php if(isset($details)){echo $details['line1'];}?>">
			 		</div>
			 	</div>
			</div>

			<div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">Line 2</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<input type="text" class="form-control no-curve" id="line2" name="line2" placeholder="Address Line 2" value="<?php if(isset($details)){echo $details['line2'];}?>">
			 		</div>
			 	</div>
			</div>

			<div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">City/Town</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<input type="text" class="form-control no-curve" id="city" name="city" placeholder="Enter City/Town" value="<?php if(isset($details)){echo $details['city'];}?>">
			 		</div>
			 	</div>
			</div>

			<div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">State</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<input type="text" class="form-control no-curve" id="state" name="state" placeholder="Enter State" value="<?php if(isset($details)){echo $details['state'];}?>">
			 		</div>
			 	</div>
			</div>

			<div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">Country</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<input type="text" class="form-control no-curve" id="country" name="country" placeholder="Enter Country" value="<?php if(isset($details)){echo $details['country'];}?>">
			 		</div>
			 	</div>
			</div>

			<div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">Postal Code</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<input type="text" class="form-control no-curve" id="pincode" name="pincode" placeholder="Enter Postal Code" value="<?php if(isset($details)){echo $details['pincode'];}?>">
			 		</div>
			 	</div>
			</div>

			<h3>Bank Account Details</h3>

			<div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">Account Number</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<input type="text" class="form-control no-curve" id="accno" name="accno" placeholder="Enter Account Number" value="<?php if(isset($details)){echo $details['bank_accno'];}?>">
			 		</div>
			 	</div>
			</div>

			<div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">IFSC Code</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<input type="text" class="form-control no-curve" id="ifsc" name="ifsc" placeholder="Enter IFSC Code" value="<?php if(isset($details)){echo $details['bank_ifsc'];}?>">
			 		</div>
			 	</div>
			</div>

			<div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">Account Type</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<input type="text" class="form-control no-curve" id="acctype" name="acctype" placeholder="Account Type" value="<?php if(isset($details)){echo $details['bank_acctype'];}?>">
			 		</div>
			 	</div>
			</div>

			<div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">Bank Name</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<input type="text" class="form-control no-curve" id="bank" name="bank" placeholder="Bank Name" value="<?php if(isset($details)){echo $details['bank_name'];}?>">
			 		</div>
			 	</div>
			</div>

			<div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">Branch</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<input type="text" class="form-control no-curve" id="branch" name="branch" placeholder="Enter Branch Name" value="<?php if(isset($details)){echo $details['bank_branch'];}?>">
			 		</div>
			 	</div>
			</div>

			 <h3>Contact Persons</h3>

			 <div class="col-md-12 col-sm-12 no-padding">
			 	<table class="table col-md-12 col-sm-12 table-bordered">
	                <tr >
	                    <th>S.No</th>
	                    <th>Name</th>
	                    <th>Gender</th>
	                    <th>Contact No</th>
	                    <th>Address</th>
	                </tr>
	                <tr >
	                    <td>1</td>
	                    <td style="padding: 0;"><input type="text" class="form-control no-curve" id="cp1_name" name="cp1_name" placeholder="Enter Name" style="margin: 0; border: 0;" value='<?php if(isset($details['c_person1']['name'])){echo $details['c_person1']['name'];}?>'></td>
	                    <td style="padding: 0;"><input type="text" class="form-control no-curve" id="cp1_gender" name="cp1_gender" placeholder="Enter Gender" style="margin: 0; border: 0;" value='<?php if(isset($details['c_person1']['gender'])){echo $details['c_person1']['gender'];}?>'></td>
	                    <td style="padding: 0;"><input type="text" class="form-control no-curve" id="cp1_mobile" name="cp1_mobile" placeholder="Enter Mobile No" style="margin: 0; border: 0;" value='<?php if(isset($details['c_person1']['contactno'])){echo $details['c_person1']['contactno'];}?>'></td>
	                    <td style="padding: 0;"><input type="text" class="form-control no-curve" id="cp1_address" name="cp1_address" placeholder="Enter Address" style="margin: 0; border: 0;" value='<?php if(isset($details['c_person1']['address'])){echo $details['c_person1']['address'];}?>'></td>
	                </tr>
	                <tr >
	                    <td>2</td>
	                    <td style="padding: 0;"><input type="text" class="form-control no-curve" id="cp2_name" name="cp2_name" placeholder="Enter Name" style="margin: 0; border: 0;" value='<?php if(isset($details['c_person1']['address'])){echo $details['c_person2']['name'];}?>'></td>
	                    <td style="padding: 0;"><input type="text" class="form-control no-curve" id="cp2_gender" name="cp2_gender" placeholder="Enter Gender" style="margin: 0; border: 0;" value='<?php if(isset($details['c_person1']['address'])){echo $details['c_person2']['gender'];}?>'></td>
	                    <td style="padding: 0;"><input type="text" class="form-control no-curve" id="cp2_mobile" name="cp2_mobile" placeholder="Enter Mobile No" style="margin: 0; border: 0;" value='<?php if(isset($details['c_person1']['address'])){echo $details['c_person2']['contactno'];}?>'></td>
	                    <td style="padding: 0;"><input type="text" class="form-control no-curve" id="cp2_address" name="cp2_address" placeholder="Enter Address" style="margin: 0; border: 0;" value='<?php if(isset($details['c_person1']['address'])){echo $details['c_person2']['address'];}?>'></td>
	                </tr>
            </table>
			 </div>

			 <h3>File Upload</h3>

			 <div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">Employee ID Card</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<input type="file" class="form-control no-curve" id="file_idcard" name="file_idcard" >
			 		</div>
			 	</div>	
			 </div>

			 <div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">Aadhaar ID</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<input type="file" class="form-control no-curve" id="file_aadhaar" name="file_aadhaar" >
			 		</div>
			 	</div>	
			 </div>

			 <div class="col-md-12 col-sm-12 form-row">
			 	<div class="col-md-6 col-sm-12">
			 		<div class="col-md-4 col-sm-4"><label class="form-lbl">Bank Passbook</label></div>
			 		<div class="col-md-8 col-sm-8">
			 			<input type="file" class="form-control no-curve" id="file_passbook" name="file_passbook" >
			 		</div>
			 	</div>	
			 </div>


		</div>

		<div class="col-md-12 col-sm-12" style="height: 100px;"></div>
		<div class="col-md-12 col-sm-12" style="position: fixed; bottom: 0; height: 50px;">
            <div class="col-md-2 col-sm-2">
                <button type="submit" class="btn btn-success">SUBMIT</button>
            </div>
        </div>

		
	</form>
</div>

<?php  include_once "footer.php";?>