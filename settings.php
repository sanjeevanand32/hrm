<?php include_once "header.php"; ?>
<div class="col-md-12 col-sm-12 page-header-div">
	<div class="col-md-10 col-sm-10 no-padding">
		<h3>Settings</h3>
	</div>
	<div class="col-md-2 col-sm-2 no-padding">
		
	</div>
    
 
</div>
<div class="col-md-12 col-sm-12 page-body">
	<div class="col-md-12 col-sm-12 no-padding" style="margin-top:20px;">
		<div class="col-md-2 col-sm-2 no-padding">
			<ul style="list-style-type: none; margin: 0;">
				<li class="pointer" style="padding: 10px; color: blue;"><strong>Employee Roles</strong></li>
				<li class="pointer" style="padding: 10px; color: blue;"><strong>Create Login</strong></li>
				<li class="pointer" style="padding: 10px; color: blue;"><strong>Change Password</strong></li>
			</ul>
		</div>
		<div class="col-md-8 col-sm-10">
			<div class="col-md-10 col-sm-10 " style="	padding: 10px;border-radius: 3px;	">
				<form class="col-md-10 col-sm-10">
					<div class="col-md-12 col-sm-12 form-row"><label>Employee Role</label></div>
					<div class="col-md-12 col-sm-12 form-row"><input type="text" id="role" class="form-control no-curve" placeholder="Enter New Role"></div>
					<div class="col-md-12 col-sm-12 form-row"><label>Description</label></div>
					<div class="col-md-12 col-sm-12 form-row"><textarea class="form-control no-curve" id="role-desc" placeholder="Enter Role Description"></textarea></div>
					<div class="col-md-12 col-sm-12 form-row"><button type="button" class="btn btn-primary btn-sm no-curve">Create</button></div>
				</form>
			</div>
			<div class="col-md-10 col-sm-10 no-padding" style="bpadding: 10px;">
				<form class="col-md-10 col-sm-10">
					<div class="col-md-12 col-sm-12 form-row"><label>Old Password</label></div>
					<div class="col-md-12 col-sm-12 form-row"><input type="password" id="old_p" class="form-control no-curve" placeholder="Enter Old Password"></div>
					<div class="col-md-12 col-sm-12 form-row"><label>New Password</label></div>
					<div class="col-md-12 col-sm-12 form-row"><input type="password" id="new_p" class="form-control no-curve" placeholder="Enter New Password"></div>
					<div class="col-md-12 col-sm-12 form-row"><label>Confirm New Password</label></div>
					<div class="col-md-12 col-sm-12 form-row"><input type="password" id="confirm_p" class="form-control no-curve" placeholder="Re-enter New Password"></div>
					<div class="col-md-12 col-sm-12 form-row"><button type="button" class="btn btn-danger btn-sm no-curve">Change Password</button></div>
				</form>
			</div>

			<div class="col-md-10 col-sm-10 no-padding" style="padding: 10px;">
				<form class="col-md-10 col-sm-10">
					<div class="col-md-12 col-sm-12 form-row"><label>Select Employee</label></div>
					<div class="col-md-12 col-sm-12 form-row">
						<select class="form-control">
							<option value="nothing">-- Select Employee --</option>
						</select>
					</div>
					<div class="col-md-12 col-sm-12 form-row"><label>Password</label></div>
					<div class="col-md-12 col-sm-12 form-row"><input type="text" id="new_p" class="form-control no-curve" placeholder="Enter New Password"></div>
					<div class="col-md-12 col-sm-12 form-row"><button type="button" class="btn btn-primary btn-sm no-curve">Create New Login</button></div>
				</f
		</div>
	</div>
</div>
<?php include_once "footer.php";?>