<!DOCTYPE html>
<html lang="en">
<head>
  <title>Forgot Password</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Lato|Montserrat|Raleway|Vollkorn&display=swap" rel="stylesheet">
  <style>
*{
    font-family: 'Vollkorn', serif;
    font-family: 'Lato', sans-serif;
    font-family: 'Raleway', sans-serif;
    font-family: 'Montserrat', sans-serif;
  } 
  body{background: #f4f5f7;} 
  .login-form-body{
      position:fixed;
      background:#fff;
      padding:30px 40px;
      margin-top: 10vh;
      box-shadow: 0 3px 16px 0 rgba(132,132,132,0.35);
  }
  .btn-span{background: #5d2bff;padding: 8px 20px;border-radius: 20px; color: #fff; font-weight: bold;}
  .no-padding{padding: 0;}
  .form-err,.eml-sent{list-style-type: none; display: none;}
  .form-err li{color: red;}
  .eml-sent{color: blue;}
  .resend-btn{display: none;}
  </style>
</head>
<body>
<div class="col-md-12 col-sm-12">
    <div class="col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4 login-form-body">
    <h3><strong>Forgot Your Password?</strong></h3>
    <p>Just type the e-mail you registered.</p>
    <div class="col-md-12 col-sm-12 no-padding">
      <ul class="form-err">
        <li>Invalid email</li>
      </ul>
    </div>
    <div class="col-md-12 col-sm-12 no-padding">
      <ul class="eml-sent">
        <li>E-mail sent. Click on Resend button if you did not receive any e-mail.</li>
      </ul>
    </div>
    <div class="form-group">
      <input type="text" class="form-control no-curve" id="eml" placeholder="Enter email">
    </div>
    <button type="button" class="btn btn-primary send-btn" onclick=sendLink()>Send</button>
    <button type="button" class="btn btn-primary resend-btn" onclick=sendLink()>Resend</button>
    <h5><a href="login.php">Go Back To Login</a></h5>
    </div>
    
</div>

</body>
</html>
<script>
function sendLink(){
  var email= $('#eml').val();
  if(email == ""){
    $('.form-err').show();
  }else{
    $('.form-err,.send-btn').hide();
    $('.eml-sent,.resend-btn').show();

  }
}
</script>